const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const exportedfn = require("../mapObject.js");

function cb(property) {
  return property + " transform";
}

const transformedObject = exportedfn(testObject, cb);
console.log(transformedObject); // New object with transformed values
console.log(testObject); // Original testObject (unchanged)
