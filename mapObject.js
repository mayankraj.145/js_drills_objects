function mapObject(obj, cb) {
  const transformedObject = {};

  for (let key in obj) {
    transformedObject[key] = cb(obj[key]);
  }

  return transformedObject; // Return the new object with transformed properties
}

module.exports = mapObject;
